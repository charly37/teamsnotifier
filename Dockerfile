FROM centos:8
MAINTAINER charles.walker.37@gmail.com

RUN dnf -y upgrade
#RUN yum -y install yum-utils
RUN dnf -y groupinstall development

#python 3
RUN dnf -y install python3
RUN dnf -y install python3-devel

#Upgrade pip
#RUN python3 -m pip install --upgrade pip

RUN mkdir /opt/mqm
RUN curl https://public.dhe.ibm.com/ibmdl/export/pub/software/websphere/messaging/mqdev/redist/9.1.0.4-IBM-MQC-Redist-LinuxX64.tar.gz -o /tmp/9.1.0.4-IBM-MQC-Redist-LinuxX64.tar.gz
RUN tar zxvf /tmp/9.1.0.4-IBM-MQC-Redist-LinuxX64.tar.gz -C /opt/mqm

#cp -rf /tmp/mq/lib64 /opt/mqm
#cp -rf /tmp/mq/inc/ /opt/mqm/inc

ENV LD_LIBRARY_PATH /opt/mqm/lib64:$LD_LIBRARY_PATH

#python 3 modules
#to talk with MQ where ticket are pushed
RUN python3 -m pip install pymqi
RUN python3 -m pip install requests

RUN dnf -y install openssh-clients


CMD [ "/bin/bash" ]