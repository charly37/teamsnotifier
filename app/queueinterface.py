import pymqi

class queueinterface:

    def __init__(self):
        self.host="mqhclust01.os.amadeus.net"
        self.port="11453"
        self.user = 'testID'
        self.password = ''
        self.queue_manager = '*'
        self.channel = 'ULAM.PUB_1A.CLUST'
        self.topic = 'mqh/aprtst/tst/#'

    def subscribe(self):
        sub_desc = pymqi.SD()
        sub_desc["Options"] = pymqi.CMQC.MQSO_CREATE + pymqi.CMQC.MQSO_RESUME + pymqi.CMQC.MQSO_MANAGED
        sub_desc.set_vs("SubName", "testID")
        sub_desc.set_vs("ObjectString", topic)
        sub_desc["SubLevel"] = 1
        sub = pymqi.Subscription(queue_manager=qmgr)
        sub.sub(sub_desc=sub_desc)

    def connect(self):
        conn_info = "%s(%s)" % (host, port)
        qmgr = pymqi.QueueManager(None)
        qmgr.connect_tcp_client(queue_manager, pymqi.CD(), channel, conn_info, user, password)
        print("Connected to %s" % conn_info)
 
        print("Successfully connected to QM=" + str(qmgr.__dict__) + " using channel=" + channel + "(" + host + ":" + port + ")")
        print("Successfully connected to " + topic + " with clientID=" + user)

 
# # Get messages
# gmo = pymqi.GMO()
# gmo.Options = pymqi.CMQC.MQGMO_FAIL_IF_QUIESCING | pymqi.CMQC.MQGMO_WAIT
# gmo.WaitInterval = 3000 # Wait up to to gmo.WaitInterval for a new message.
 
# queue = sub.get_sub_queue()
 
# keep_running = True
# md = pymqi.MD()
# while keep_running:
#     try:
#         # Wait up to to gmo.WaitInterval for a new message.
#         message = queue.get(None, md, gmo)
#         print(message)
#         # Process the message here..
 
#         # Reset the MsgId, CorrelId & GroupId so that we can reuse
#         # the same 'md' object again.
#         md.MsgId = pymqi.CMQC.MQMI_NONE
#         md.CorrelId = pymqi.CMQC.MQCI_NONE
#         md.GroupId = pymqi.CMQC.MQGI_NONE
 
#     except pymqi.MQMIError as e:
#         if e.comp == pymqi.CMQC.MQCC_FAILED and e.reason == pymqi.CMQC.MQRC_NO_MSG_AVAILABLE:
#             # No messages, that's OK, we can ignore it.
#             pass
#         else:
#             # Some other error condition.
#             raise
 
# sub.close(close_sub_queue=True)
# qmgr.disconnect()

