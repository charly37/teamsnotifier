import requests
import json


class teamsnotifier:

    def __init__(self):
        self.url = "https://outlook.office.com/webhook/666"

    def post(self):

        aCard = {
            "@context": "https://schema.org/extensions",
            "@type": "MessageCard",
            "themeColor": "0072C6",
            "title": "Mew Win@proach notification",
            "text": "Click **Open** to get more details",
            "potentialAction": [
                {
                    "@type": "OpenUri",
                    "name": "Open",
                    "targets": [
                        {
                            "os": "default",
                            "uri": "https://www.amadeus.com"
                        }
                    ]
                }
            ]
        }

        response = requests.request("POST", self.url, data=json.dumps(aCard))

        print(response.text)



